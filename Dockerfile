FROM node:13.12.0-alpine
WORKDIR /app
COPY . /app/
RUN npm install --silent --only=prod
EXPOSE 80
CMD ["npm", "start"]
