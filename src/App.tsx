import React from 'react';
import { ApolloClient, ApolloProvider, InMemoryCache } from '@apollo/client';
import './App.scss';
import { Users } from './modules/users';

const client = new ApolloClient({
  uri: process.env.REACT_APP_API_URL,
  cache: new InMemoryCache(),
});

const App = () => {
  return (
    <div className="app">
      <ApolloProvider client={client}>
        <Users />
      </ApolloProvider>
    </div>
  );
};

export default App;
