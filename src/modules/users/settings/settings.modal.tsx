import React, { FC, useCallback, useMemo, useRef } from 'react';
import { Modal, Form, Checkbox, Button } from 'semantic-ui-react';
import { Formik, FormikProps } from 'formik';

import styles from './settings.module.scss';

type ComponentProps = {
  visible: boolean;
  onClose: () => void;
  columns: Map<string, boolean>;
  updateTableColumns: (values: SettingsFormValues) => void;
};

export const SettingsModal: FC<ComponentProps> = ({
  visible,
  onClose,
  columns,
  updateTableColumns,
}) => {
  const formRef = useRef<FormikProps<SettingsFormValues>>();

  const initialValues = useMemo<SettingsFormValues>(() => {
    return {
      firstname: columns.get('firstname'),
      lastname: columns.get('lastname'),
      age: columns.get('age'),
      email: columns.get('email'),
      active: columns.get('active'),
      married: columns.get('married'),
      verified: columns.get('verified'),
    };
  }, [columns]);

  const handleFormSubmit = useCallback(
    (values: SettingsFormValues) => {
      updateTableColumns(values);
    },
    [updateTableColumns],
  );

  return (
    <Modal onClose={onClose} open={visible} size="mini">
      <Modal.Header>Columns Selection</Modal.Header>
      <Modal.Content className={styles.container}>
        <Formik initialValues={initialValues} onSubmit={handleFormSubmit} innerRef={formRef}>
          {({ values, setFieldValue }) => (
            <Form>
              <div className={styles.row}>
                <div className={styles.title}>FirstName</div>
                <Checkbox
                  toggle
                  checked={values.firstname}
                  onChange={(event, data) => setFieldValue('firstname', data.checked)}
                />
              </div>
              <div className={styles.row}>
                <div className={styles.title}>LastName</div>
                <Checkbox
                  toggle
                  checked={values.lastname}
                  onChange={(event, data) => setFieldValue('lastname', data.checked)}
                />
              </div>
              <div className={styles.row}>
                <div className={styles.title}>Age</div>
                <Checkbox
                  toggle
                  checked={values.age}
                  onChange={(event, data) => setFieldValue('age', data.checked)}
                />
              </div>
              <div className={styles.row}>
                <div className={styles.title}>Email</div>
                <Checkbox
                  toggle
                  checked={values.email}
                  onChange={(event, data) => setFieldValue('email', data.checked)}
                />
              </div>
              <div className={styles.row}>
                <div className={styles.title}>Active</div>
                <Checkbox
                  toggle
                  checked={values.active}
                  onChange={(event, data) => setFieldValue('active', data.checked)}
                />
              </div>
              <div className={styles.row}>
                <div className={styles.title}>Married</div>
                <Checkbox
                  toggle
                  checked={values.married}
                  onChange={(event, data) => setFieldValue('married', data.checked)}
                />
              </div>
              <div className={styles.row}>
                <div className={styles.title}>Verified</div>
                <Checkbox
                  toggle
                  checked={values.verified}
                  onChange={(event, data) => setFieldValue('verified', data.checked)}
                />
              </div>
            </Form>
          )}
        </Formik>
      </Modal.Content>
      <Modal.Actions>
        <Button type="button" onClick={onClose}>
          Cancel
        </Button>
        <Button
          primary
          onClick={() => {
            formRef.current.submitForm();
            onClose();
          }}
        >
          Save
        </Button>
      </Modal.Actions>
    </Modal>
  );
};
