import React, { FC, useMemo, useState } from 'react';
import { Button, Icon, Table } from 'semantic-ui-react';

import { SettingsModal } from './settings';
import { FiltersModal } from './filters';
import styles from './users.module.scss';

type ComponentProps = {
  users: User[];
  tableColumns: TableColumn[];
  updateTableColumns: (values: SettingsFormValues) => void;
  filterUsers: (variables: UserFilterVariables) => void;
};

export const Users: FC<ComponentProps> = ({
  users,
  filterUsers,
  updateTableColumns,
  tableColumns,
}) => {
  const [showSettings, setShowSettings] = useState(false);
  const [showFilters, setShowFilters] = useState(false);

  const columns = useMemo<Map<string, boolean>>(() => {
    const columnsMap = new Map();
    // eslint-disable-next-line no-restricted-syntax
    for (const column of tableColumns) {
      columnsMap.set(column.name, column.enabled);
    }

    return columnsMap;
  }, [tableColumns]);

  return (
    <div className={styles.container}>
      <div className={styles.header}>
        <div className={styles.title}>Balkan.id Users</div>
        <div className={styles.buttonsSection}>
          <Button icon type="button" onClick={() => setShowFilters(true)}>
            Filters &nbsp; <Icon name="filter" />
          </Button>
          <Button icon type="button" onClick={() => setShowSettings(true)}>
            Settings &nbsp; <Icon name="settings" />
          </Button>
        </div>
      </div>
      <div className={styles.content}>
        <div className={styles.tableContainer}>
          <Table>
            <Table.Header>
              <Table.Row>
                {columns.get('firstname') && <Table.HeaderCell>FirstName</Table.HeaderCell>}
                {columns.get('lastname') && <Table.HeaderCell>LastName</Table.HeaderCell>}
                {columns.get('age') && <Table.HeaderCell>Age</Table.HeaderCell>}
                {columns.get('email') && <Table.HeaderCell>Email</Table.HeaderCell>}
                {columns.get('active') && <Table.HeaderCell>Active</Table.HeaderCell>}
                {columns.get('married') && <Table.HeaderCell>Married</Table.HeaderCell>}
                {columns.get('verified') && <Table.HeaderCell>Verified</Table.HeaderCell>}
              </Table.Row>
            </Table.Header>
            <Table.Body>
              {users.map((user) => (
                <Table.Row key={user.id}>
                  {columns.get('firstname') && <Table.Cell>{user.firstname}</Table.Cell>}
                  {columns.get('lastname') && <Table.Cell>{user.lastname}</Table.Cell>}
                  {columns.get('age') && <Table.Cell>{user.age}</Table.Cell>}
                  {columns.get('email') && <Table.Cell>{user.email}</Table.Cell>}
                  {columns.get('active') && (
                    <Table.Cell>
                      {user.active ? (
                        <Icon name="check circle outline" size="large" color="green" />
                      ) : (
                        <Icon name="times circle outline" size="large" color="grey" />
                      )}
                    </Table.Cell>
                  )}
                  {columns.get('married') && (
                    <Table.Cell>
                      {user.married ? (
                        <Icon name="check circle outline" size="large" color="green" />
                      ) : (
                        <Icon name="times circle outline" size="large" color="grey" />
                      )}
                    </Table.Cell>
                  )}
                  {columns.get('verified') && (
                    <Table.Cell>
                      {user.verified ? (
                        <Icon name="check circle outline" size="large" color="green" />
                      ) : (
                        <Icon name="times circle outline" size="large" color="grey" />
                      )}
                    </Table.Cell>
                  )}
                </Table.Row>
              ))}
            </Table.Body>
          </Table>
        </div>
      </div>
      <SettingsModal
        visible={showSettings}
        onClose={() => setShowSettings(false)}
        columns={columns}
        updateTableColumns={updateTableColumns}
      />
      <FiltersModal
        visible={showFilters}
        onClose={() => setShowFilters(false)}
        fetchData={filterUsers}
      />
    </div>
  );
};
