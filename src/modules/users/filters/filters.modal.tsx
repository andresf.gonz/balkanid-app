import React, { FC, useCallback, useRef } from 'react';
import { Modal, Form, DropdownItemProps, Button } from 'semantic-ui-react';
import { InputField } from 'components/input-field';
import { SelectorField } from 'components/selector-field';
import { FastField, Formik, FormikHelpers, FormikProps } from 'formik';

import styles from './filters.module.scss';

type ComponentProps = {
  visible: boolean;
  onClose: () => void;
  fetchData: (filters: UserFilterVariables) => void;
};

const initialValues: UserFilterVariables = {
  firstname: '',
  lastname: '',
  age: '' as any,
  email: '',
  active: null,
  married: null,
  verified: null,
};

const booleanOptions: DropdownItemProps[] = [
  {
    key: 'all',
    value: null,
    text: 'All',
  },
  {
    key: 'yes',
    value: true,
    text: 'Yes',
  },
  {
    key: 'no',
    value: false,
    text: 'No',
  },
];

export const FiltersModal: FC<ComponentProps> = ({ fetchData, visible, onClose }) => {
  const formRef = useRef<FormikProps<UserFilterVariables>>();
  const handleFormSubmit = useCallback(
    (values: UserFilterVariables, helpers: FormikHelpers<UserFilterVariables>) => {
      const request = { ...values };

      if (values.firstname.trim() === '') request.firstname = null;
      if (values.lastname.trim() === '') request.lastname = null;
      if (values.age.toString().trim() === '') request.age = null;
      else values.age = parseFloat(request.age.toString());
      if (values.email.trim() === '') request.email = null;

      fetchData(request);
      helpers.resetForm();

      onClose();
    },
    [fetchData],
  );

  return (
    <Modal onClose={onClose} open={visible} size="mini">
      <Modal.Header>Filters</Modal.Header>
      <Modal.Content className={styles.container}>
        <Formik initialValues={initialValues} onSubmit={handleFormSubmit} innerRef={formRef}>
          {({ values }) => (
            <Form>
              <FastField
                name="firstname"
                label="FirstName"
                placeholder="Firstname Filter"
                component={InputField}
              />
              <FastField
                name="lastname"
                label="LastName"
                placeholder="LastName Filter"
                component={InputField}
              />
              <FastField name="age" label="Age" placeholder="Age Filter" component={InputField} />
              <FastField
                name="email"
                label="Email"
                placeholder="Email Filter"
                component={InputField}
              />
              <FastField
                name="active"
                label="Active"
                options={booleanOptions}
                value={values.active}
                component={SelectorField}
                placeholder="All"
              />
              <FastField
                name="married"
                label="Married"
                options={booleanOptions}
                value={values.married}
                component={SelectorField}
                placeholder="All"
              />
              <FastField
                name="verified"
                label="Verified"
                options={booleanOptions}
                value={values.verified}
                component={SelectorField}
                placeholder="All"
              />
            </Form>
          )}
        </Formik>
      </Modal.Content>
      <Modal.Actions>
        <Button type="button" onClick={onClose}>
          Cancel
        </Button>
        <Button
          primary
          onClick={() => {
            formRef.current.submitForm();
          }}
        >
          Apply
        </Button>
      </Modal.Actions>
    </Modal>
  );
};
