import React, { FC, useCallback } from 'react';
import { useUsers } from 'graphql/users';
import { useTableColumns } from 'graphql/table-columns';
import { Users as Component } from './users';

export const Users: FC = () => {
  const { users, refetch } = useUsers();
  const { tableColumns, updateTableColumns } = useTableColumns();

  const filterUsers = useCallback(
    async (variables: UserFilterVariables) => {
      await refetch(variables);
    },
    [refetch],
  );

  return (
    <Component
      users={users}
      filterUsers={filterUsers}
      tableColumns={tableColumns}
      updateTableColumns={updateTableColumns}
    />
  );
};
