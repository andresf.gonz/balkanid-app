declare type User = {
  id?: string;
  firstname: string;
  lastname: string;
  age: number;
  married: boolean;
  active: boolean;
  verified: boolean;
  email: string;
};

declare type TableColumn = {
  id?: string;
  name?: string;
  enabled: boolean;
};

declare type UpdateTableColumns = {
  id: string;
  enabled: boolean;
}[];

declare type UserFilterVariables = {
  firstname: string;
  lastname: string;
  age: number;
  email: string;
  active: boolean;
  married: boolean;
  verified: boolean;
};

declare type SettingsFormValues = {
  firstname: boolean;
  lastname: boolean;
  age: boolean;
  email: boolean;
  active: boolean;
  married: boolean;
  verified: boolean;
};
