import { gql } from '@apollo/client';

export const allTableColumnsGQL = gql`
  query allTableColumns {
    tableColumns: allTableColumns {
      id
      name
      enabled
    }
  }
`;

export const updateTableColumnsGQL = gql`
  mutation updateTableColumns($columnsValue: [TableColumnUpdateInputType!]!) {
    tableColumns: updateTableColumns(columnsValue: $columnsValue) {
      id
      name
      enabled
    }
  }
`;
