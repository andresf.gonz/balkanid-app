import { useCallback } from 'react';
import { useQuery, useMutation } from '@apollo/client';
import { allTableColumnsGQL, updateTableColumnsGQL } from './table-columns.gql';

export const useTableColumns = () => {
  const { data } = useQuery<{ tableColumns: TableColumn[] }>(allTableColumnsGQL);
  const [updateTableColumnsMutation] = useMutation<null, { columnsValue: UpdateTableColumns }>(
    updateTableColumnsGQL,
  );

  const updateTableColumns = useCallback(
    async (values: { [key: string]: boolean }) => {
      const columnsValue: UpdateTableColumns = data.tableColumns.map((column) => ({
        id: column.id,
        enabled: values[column.name],
      }));

      await updateTableColumnsMutation({ variables: { columnsValue } });
    },
    [updateTableColumnsMutation, data],
  );

  return {
    tableColumns: data?.tableColumns ?? [],
    updateTableColumns,
  };
};
