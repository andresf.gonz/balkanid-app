import { useQuery } from '@apollo/client';
import { allUsersGQL } from './users.gql';

export const useUsers = () => {
  const { data, refetch } = useQuery<{ users: User[] }, UserFilterVariables>(allUsersGQL, {
    variables: {
      age: null,
      active: null,
      email: null,
      lastname: null,
      firstname: null,
      married: null,
      verified: null,
    },
  });

  return {
    users: data?.users ?? [],
    refetch,
  };
};
