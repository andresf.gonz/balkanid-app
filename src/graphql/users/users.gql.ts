import { gql } from '@apollo/client';

export const allUsersGQL = gql`
  query allUsers(
    $firstname: String
    $lastname: String
    $age: Float
    $email: String
    $active: Boolean
    $married: Boolean
    $verified: Boolean
  ) {
    users: allUsers(
      firstname: $firstname
      lastname: $lastname
      age: $age
      email: $email
      active: $active
      married: $married
      verified: $verified
    ) {
      id
      firstname
      lastname
      age
      email
      active
      married
      verified
    }
  }
`;
