import React, { FC } from 'react';
import { ErrorMessage, FastFieldProps } from 'formik';
import { Form, Icon, Popup } from 'semantic-ui-react';
import fieldStyles from 'styles/form-field.module.scss';
import styles from './input-field.module.scss';

export type ComponentProps = {
  label: string;
  type?: string;
  placeholder?: string;
  infoMessage?: string;
} & FastFieldProps;

export const InputField: FC<ComponentProps> = React.memo(
  ({ field, form, label, type, placeholder, infoMessage }) => (
    <div className={fieldStyles.container}>
      <Form.Input
        {...field}
        label={
          <div className={styles.labelContainer}>
            <label>{label}</label>
            {infoMessage && (
              <Popup
                content={infoMessage}
                header={label}
                trigger={<Icon name="info circle" className={styles.infoIcon} />}
              />
            )}
          </div>
        }
        type={type}
        placeholder={placeholder}
        fluid
      />
      {form.errors[field.name] && form.touched[field.name] && (
        <span className={fieldStyles.errorMessage}>
          <ErrorMessage name={field.name} />
        </span>
      )}
    </div>
  ),
);

InputField.defaultProps = {
  type: 'text',
  placeholder: '',
};
